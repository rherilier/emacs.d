;;; metabuild-guess.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-19
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the guessing code of `metabuild'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-when-compile
  (require 'cl-lib)
  (require 'ido))

(require 'metabuild-utils)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild--guess-candidates (root-path marker &optional marker-filter)
  "Search recursively in ROOT-PATH for a all files named MARKER.
If non-nil, MARKER-FILTER will be used to reject candidates."
  (let* ((marker-is-dir (directory-name-p marker))
         (marker-as-file-name (if marker-is-dir
                                  (directory-file-name marker)
                                marker))
         (files (let ((all-files (directory-files-recursively root-path
                                                              (concat marker-as-file-name "\\'")
                                                              marker-is-dir)))
                  (if marker-filter
                      (cl-remove-if-not marker-filter all-files)
                    all-files)))
         (files-with-dir-count (cl-sort (mapcar (lambda (f)
                                                  (list (cl-list-length (file-name-split f))
                                                        f))
                                                files)
                                        #'<
                                        :key #'cl-first))
         (min-len (car (car files-with-dir-count)))
         (candidates (mapcar (lambda (e)
                               (let ((p (cl-second e)))
                                 (file-name-directory (if marker-is-dir
                                                          (directory-file-name p)
                                                        p))))
                             (cl-remove-if (lambda (e)
                                             (not (eq (car e) min-len)))
                                           files-with-dir-count))))
    candidates))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild--guess-srcs (build-sys root-path)
  (let* ((marker (metabuild-build-sys-marker build-sys))
         (marker-filter (metabuild-build-sys-marker-filter build-sys)))
    (metabuild--guess-candidates root-path marker marker-filter)))

(defun metabuild--guess-builds (build-sys root-path)
  (when-let* ((build-marker (metabuild-build-sys-build-marker build-sys)))
    (metabuild--guess-candidates root-path build-marker)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-guess ()
  "Try to guess the current project and the available builds.
If more than one top-level source directory is found, the one to consider
will be asked for. If more than one build directory is found, the one to
consider will be asked for."
  (interactive)
  (if-let* ((project (project-current))
            (root-path (project-root project))
            (build-sys (metabuild--guess-build-sys root-path))
            (src-paths (metabuild--guess-srcs build-sys root-path))
            (src-dirs (metabuild-relative-names root-path src-paths))
            (src-dir (metabuild-choose "which source directory? " src-dirs))
            (src-path (expand-file-name src-dir root-path))
            (build-paths (metabuild--guess-builds build-sys root-path))
            (build-dirs (metabuild-relative-names root-path build-paths))
            (build-dir (metabuild-choose "which build directory? " build-dirs))
            (build-path (expand-file-name build-dir root-path)))
      (progn (metabuild--install-builder build-sys nil root-path src-path build-path)
             (run-hooks 'metabuild-reconfigure-hook))
    (message "no build found in %s, you will have to initialize one."
             default-directory)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-guess)
;;; metabuild-guess.el ends here
