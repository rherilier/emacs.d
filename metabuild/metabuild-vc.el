;;; metabuild-vc.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-20
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the VCS related code  of `metabuild'

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-when-compile
  (require 'cl-lib)
  (require 'project)
  (require 'vc))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-utils)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconst common-ignored-files '("~$" "\\.#" "\\.swp$"))
(defconst common-ignored-dirs '("^/usr"))
(defconst common-ignored-all (append common-ignored-files common-ignored-dirs))

(defconst git-garbage-files '("\\.orig$" "_BACKUP_[0-9]*" "_BASE_[0-9]*" "_LOCAL_[0-9]*" "_REMOTE_[0-9]*"))

(defconst git-list-tracked "git ls-files -zc")
(defconst git-list-untracked "git ls-files -zo --directory --no-empty-directory")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-vc-files (project)
  "Return the list of files registered in the VCS pointed by PROJECT."
  (if-let* ((vcs (cl-second project))
            (root-path (project-root project)))
      (cond
       ((eq vcs 'Git)
        (metabuild-list-from-shell root-path git-list-tracked))
       (t
        (cl-remove-if-not #'vc-registered
                          (project-files project))))
    (cl-remove-if-not #'vc-registered
                      (project-files project))))

(defun metabuild-vc-list-ignored-dirs (project)
  "Return the list of ignored directories the VCS pointed by PROJECT."
  (if-let* ((vcs (cl-second project))
            (root-path (project-root project)))
      (cond
       ((eq vcs 'Git)
        (cl-remove-if-not #'directory-name-p
                          (metabuild-list-from-shell root-path
                                                     git-list-untracked)))
       (t
        common-ignored-dirs))
    common-ignored-dirs))

(defun metabuild-vc-list-ignored-all (project)
  "Return the list of ignored files or directories in the VCS pointed by PROJECT."
  (if-let* ((vcs (cl-second project))
            (root-path (project-root project)))
      (cond
       ((eq vcs 'Git)
        (append common-ignored-all
                git-garbage-files
                (cl-remove-if-not #'directory-name-p
                                  (metabuild-list-from-shell root-path
                                                             git-list-untracked))))
       (t
        common-ignored-all))
    common-ignored-all))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-vc)
;;; metabuild-vc.el ends here
