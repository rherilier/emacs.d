;;; metabuild-sys-meson.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-20
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the implementation of `metabuild' for the Meson build system

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-utils)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-sys--meson-configure (build-type src-path build-path)
  (list "meson" "setup"
        (when build-type
          (list "--buildtype" build-type))
        (metabuild-make-path build-path)
        (metabuild-make-path src-path)))

(defun metabuild-sys--meson-reconfigure (src-path build-path)
  (list "meson" "setup" "--wipe"
        (metabuild-make-path build-path)
        (metabuild-make-path src-path)))

(defun metabuild-sys--meson-build (build-path)
  (list "meson" "compile"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--meson-clean (build-path)
  (list "meson" "compile" "--clean"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--meson-test (build-path)
  (list "meson" "test"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--meson-install (build-path)
  (list "meson" "install"
        "-C" (metabuild-make-path build-path)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;###autoload
(metabuild-register-build-system 'meson
  :marker
  "meson.build"
  :marker-filter
  #'(lambda (path) (not (string-match-p "subprojects/" path)))
  :build-types
  '("debug" "debugoptimized" "release" "plain" "minsize" "custom")
  :build-marker
  "meson-info/"
  :cmd-configure
  #'metabuild-sys--meson-configure
  :cmd-reconfigure
  #'metabuild-sys--meson-reconfigure
  :cmd-build
  #'metabuild-sys--meson-build
  :cmd-clean
  #'metabuild-sys--meson-clean
  :cmd-test
  #'metabuild-sys--meson-test
  :cmd-install
  #'metabuild-sys--meson-install
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-sys-meson)
;;; metabuild-sys-meson.el ends here
