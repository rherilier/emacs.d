;;; metabuild-sys-make.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-21
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the implementation of `metabuild' for the Make command

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-utils)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;###autoload
(metabuild-register-build-system 'make
  :out-of-tree
  nil
  :marker
  "Makefile"
  :build-marker
  "Makefile"
  :cmd-build
  (list "bear" "--" "make")
  :cmd-clean
  (list "bear" "--" "make" "clean")
  :cmd-test
  (list "bear" "--" "make" "test")
  :cmd-install
  (list "bear" "--" "make" "install")
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-sys-make)
;;; metabuild-sys-make.el ends here
