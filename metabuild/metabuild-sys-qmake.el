;;; metabuild-sys-qmake.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-21
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the implementation of `metabuild' for the QMake

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-utils)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-sys--qmake-configure (build-type src-path build-path)
  (list "qmake"
        "-o" (metabuild-make-path build-path "Makefile")))

(defun metabuild-sys--qmake-build (build-path)
  (list "bear" "--" "make"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--qmake-clean (build-path)
  (list "bear" "--" "make" "clean"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--qmake-test (build-path)
  (list "bear" "--" "make" "test"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--qmake-install (build-path)
  (list "bear" "--" "make" "install"
        "-C" (metabuild-make-path build-path)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;###autoload
(metabuild-register-build-system 'qmake
  :marker
  ".pro$"
  :build-marker
  ".qmake.stash"
  :cmd-configure
  #'metabuild-sys--qmake-configure
  :cmd-build
  #'metabuild-sys--qmake-build
  :cmd-clean
  #'metabuild-sys--qmake-clean
  :cmd-test
  #'metabuild-sys--qmake-test
  :cmd-install
  #'metabuild-sys--qmake-install
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-sys-qmake)
;;; metabuild-sys-qmake.el ends here
