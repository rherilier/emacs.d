;;; metabuild-core.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-19
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the core code of `metabuild'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-when-compile
  (require 'cl-lib)
  (require 'project))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild--concat-symbol (prefix name)
  (intern (concat prefix (symbol-name name))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgroup metabuild nil
  "Another build systems abstraction."
  :group 'initialization)

(defcustom metabuild-default-build-dir "build"
  "The default name of the build directory when feasable."
  :type 'string
  :group 'metabuild)

(defcustom metabuild-use-dedicated-type-dir t
  "If non-nil, use a dedicated directory for each build type.
The extra directory name in the path is the build type as provided by the
build system."
  :type 'boolean
  :group 'metabuild)

(defcustom metabuild-reconfigure-hook nil
  "List of functions to be called after a project has been (re)configured"
  :type 'hook
  :group 'metabuild)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-defstruct metabuild-build-sys
  name
  (out-of-tree t)
  marker
  marker-filter
  build-types
  build-marker
  cmd-configure
  cmd-reconfigure
  cmd-build
  cmd-clean
  cmd-test
  cmd-install
  cmd-package)

(defvar metabuild-build-systems nil
  "The list of supported build systems")

(defmacro metabuild-register-build-system (name &rest args)
  "Register a new build system.
Usage:

  (metabuild-register-build-system name
    [:keyword [option]]...)

:name                 The build system's name
:out-of-tree          The build system can operate out of the source tree (default to true)
:marker               The build system's configuration marker (file or directory)
:marker-filter        A filter to reject irrelevant marker files
:build-types          The list of the supported build types' name
:build-marker         The build system's build marker (file or directory)
:cmd-configure        The command to run the configuration stage
:cmd-reconfigure      The command to clean/fix the configuration stage
:cmd-build            The command to run the build stage
:cmd-clean            The command to clean the build stage
:cmd-test             The command to run the test stage
:cmd-install          The command to run the installation stage
:cmd-package          The command to run the packaging stage
:disabled             To disable it"
  (declare (indent defun))
  (unless (memq :disabled args)
    `(add-to-list 'metabuild-build-systems
                  (make-metabuild-build-sys :name ,name ,@args))))

(defun metabuild--get-cmd (build-sys name &rest args)
  (when-let* ((slot (metabuild--concat-symbol ":shell-" name))
              (cmd (funcall (metabuild--concat-symbol "metabuild-build-sys-cmd-"
                                                      name)
                            build-sys)))
    (list slot (mapconcat 'identity
                          (flatten-tree (if (functionp cmd)
                                            (apply cmd args)
                                          cmd))
                          " "))))

(defun metabuild--guess-build-sys (path)
  (cl-loop for b in metabuild-build-systems
           do (if-let* ((marker (metabuild-build-sys-marker b))
                        (files (directory-files path nil marker)))
                  (cl-return b))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-defstruct metabuild-builder
  name
  root-path
  build-path
  shell-configure
  shell-reconfigure
  shell-build
  shell-clean
  shell-test
  shell-install
  shell-package)

(defvar metabuild-builders-map (make-hash-table :test 'equal)
  "The metabuild's build system instances.
The KEY is the project's root directory and the VALUE is a builder.")

(defun metabuild--create-builder (build-sys type root-path src-path build-path)
  (let* ((name (metabuild-build-sys-name build-sys))
         (build-cmd (metabuild--get-cmd build-sys 'build build-path))
         (args (append `(:name ,name)
                       `(:root-path ,root-path)
                       `(:build-path ,build-path)
                       (metabuild--get-cmd build-sys 'configure type src-path build-path)
                       (metabuild--get-cmd build-sys 'reconfigure src-path build-path)
                       build-cmd
                       (metabuild--get-cmd build-sys 'clean build-path)
                       (metabuild--get-cmd build-sys 'test build-path)
                       (metabuild--get-cmd build-sys 'install build-path)
                       (metabuild--get-cmd build-sys 'package build-path))))
    (when-let* ((cmd (cl-second build-cmd)))
      (setq compile-command cmd))
    (apply #'make-metabuild-builder args)))

(defun metabuild--install-builder (build-sys type root-path src-path build-path)
  (puthash root-path
           (eval (metabuild--create-builder build-sys type root-path src-path build-path))
           metabuild-builders-map))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-get-current-builder ()
  "Return the builder for the current project or nil."
  (when-let* ((project (project-current))
              (root-path (project-root project)))
    (gethash root-path metabuild-builders-map)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-current-builder-build-path ()
  "Get the builder's build-path of the current project or nil."
  (interactive)
  (when-let* ((builder (metabuild-get-current-builder)))
    (metabuild-builder-build-path builder)))

(defun metabuild-current-builder-root-path ()
  "Get the builder's root-path of the current project or nil."
  (interactive)
  (when-let* ((builder (metabuild-get-current-builder)))
    (metabuild-builder-root-path builder)))

(defun metabuild-get-current-builder-cmd-p (name)
  "Return the builder for the current project or nil."
  (when-let* ((builder (metabuild-get-current-builder))
              (cmd (metabuild--concat-symbol "metabuild-builder-shell-"  name))
              (fun (funcall cmd builder)))
    t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-core)
;;; metabuild-core.el ends here
