;;; metabuild-file.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-20
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the files related code of `metabuild'

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-utils)
(require 'metabuild-vc)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-find-project-file ()
  "Jump to a project's file using completion."
  (interactive)
  (when-let* ((project (project-current))
              (root-path (expand-file-name (project-root project)))
              (files (project-files project))
              (file (metabuild-completing-read "Find file: "
                                               files)))
    (find-file (expand-file-name file root-path))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-find-vc-file ()
  "Jump to a project's registered file using completion."
  (interactive)
  (when-let* ((project (project-current))
              (root-path (expand-file-name (project-root project)))
              (files (metabuild-vc-files project))
              (file (metabuild-completing-read "Find file: "
                                               files)))
    (find-file (expand-file-name file root-path))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-file)
;;; metabuild-file.el ends here
