;;; metabuild-sys-autotools.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-21
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the implementation of `metabuild' for the autotools

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-utils)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-sys--autotools-configure (build-type src-path build-path)
  (list (metabuild-make-path src-path "configure")))

(defun metabuild-sys--autotools-build (build-path)
  (list "bear" "--" "make"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--autotools-clean (build-path)
  (list "bear" "--" "make" "clean"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--autotools-test (build-path)
  (list "bear" "--" "make" "test"
        "-C" (metabuild-make-path build-path)))

(defun metabuild-sys--autotools-install (build-path)
  (list "bear" "--" "make" "install"
        "-C" (metabuild-make-path build-path)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;###autoload
(metabuild-register-build-system 'autotools
  :marker
  "configure"
  :build-marker
  "config.cache"
  :cmd-configure
  #'metabuild-sys--autotools-configure
  :cmd-build
  #'metabuild-sys--autotools-build
  :cmd-clean
  #'metabuild-sys--autotools-clean
  :cmd-test
  #'metabuild-sys--autotools-test
  :cmd-install
  #'metabuild-sys--autotools-install
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-sys-autotools)
;;; metabuild-sys-autotools.el ends here
