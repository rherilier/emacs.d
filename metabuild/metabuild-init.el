;;; metabuild-init.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-22
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the build initialization code of `metabuild'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-guess)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-initialize-build ()
  "Configure a new build for the current project."
  (interactive)
  (if-let* ((project (project-current))
            (root-path (project-root project))
            (build-sys (metabuild--guess-build-sys root-path))
            (out-of-tree (metabuild-build-sys-out-of-tree build-sys))
            (src-dirs (metabuild-relative-names root-path
                                                (metabuild--guess-srcs build-sys
                                                                       root-path)))
            (src-dir (metabuild-choose "which source directory? "
                                       src-dirs))
            (src-path (expand-file-name root-path src-dir)))
      (let* ((build-types (metabuild-build-sys-build-types build-sys))
             (build-type  (if build-types
                              (if (listp build-types)
                                  (metabuild-completing-read "build type: "
                                                             build-types)
                                build-types)
                            nil))
             (build-path (if out-of-tree
                             (file-name-concat root-path metabuild-default-build-dir
                                               (when build-type
                                                 build-type))
                           nil)))
        (metabuild--install-builder build-sys build-type root-path src-path build-path)
        (make-directory build-path t)
        (metabuild-run-configure))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-init)
;;; metabuild-init.el ends here
