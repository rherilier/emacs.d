;;; metabuild.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-19
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;; To use it:
;; * call metabuild-initialize-build and answer some questions;
;; * call metabuild-guess to find what's available and answer some question when needed.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-vc)
(require 'metabuild-utils)
(require 'metabuild-file)
(require 'metabuild-guess)
(require 'metabuild-init)
(require 'metabuild-commands)
(require 'metabuild-sys-make)
(require 'metabuild-sys-qmake)
(require 'metabuild-sys-autotools)
(require 'metabuild-sys-cmake)
(require 'metabuild-sys-meson)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild)
;;; metabuild.el ends here

