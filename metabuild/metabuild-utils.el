;;; metabuild-utils.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-19
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains some utilities for `metabuild'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'ido)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro metabuild-make-path (path &rest args)
  "Return a path usable in a shell command."
  `(cl-prin1-to-string (expand-file-name (if ,@args
                                             (file-name-concat ,path ,@args)
                                           ,path))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-relative-names (root-path files)
  "Make all files from FILES relative to ROOT-PATH."
  (mapcar (lambda (f)
            (string-replace root-path "" f))
          files))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-completing-read (prompt choices &optional exact)
  (ido-completing-read prompt choices
                       nil exact))

(defun metabuild-choose (prompt choices &optional exact)
  "Ask the user to choose among a set of CHOICES and return it.
If there is only one choice, it is returned immediatly."
  (if (eq (cl-list-length choices)
          1)
      (car choices)
    (metabuild-completing-read prompt choices exact)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-list-from-shell (root-path command)
  "Execute COMMAND in the directory ROOT-PATH and return the output as a list
of lines. The command must use '\0' as line separator."
  (when (stringp command)
    (let ((default-directory root-path))
      (with-temp-buffer
        (shell-command command t "*metabuild-list-errors*")
        (let ((shell-output (buffer-substring (point-min) (point-max))))
          (split-string (string-trim shell-output) "\0" t))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-utils)
;;; metabuild-utils.el ends here

