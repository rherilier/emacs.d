;;; metabuild-sys-cmake.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-20
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the implementation of `metabuild' for the CMake build system

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)
(require 'metabuild-utils)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-sys--cmake-configure (build-type src-path build-path)
  (list "cmake"
        (when build-type
          (format "-DCMAKE_BUILD_TYPE=%s" build-type))
        "-DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
        "-B" (metabuild-make-path build-path)
        "-S" (metabuild-make-path src-path)))

(defun metabuild-sys--cmake-reconfigure (src-path build-path)
  (list "cmake" "--fresh"
        "-B" (metabuild-make-path build-path)
        "-S" (metabuild-make-path src-path)))

(defun metabuild-sys--cmake-build (build-path)
  (list "cmake"
        "--build" (metabuild-make-path build-path)))

(defun metabuild-sys--cmake-clean (build-path)
  (list "cmake"
        "--build" (metabuild-make-path build-path)
        "--target" "clean"))

(defun metabuild-sys--cmake-test (build-path)
  (list "cmake"
        "--build" (metabuild-make-path build-path)
        "--target" "test"))

(defun metabuild-sys--cmake-install (build-path)
  (list "cmake" "--install"
        (metabuild-make-path build-path)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;###autoload
(metabuild-register-build-system 'cmake
  :marker
  "CMakeLists.txt"
  :build-types
  '("Debug" "Release" "RelWithDebInfo" "MinSizeRel" "None")
  :build-marker
  "CMakeCache.txt"
  :cmd-configure
  #'metabuild-sys--cmake-configure
  :cmd-reconfigure
  #'metabuild-sys--cmake-reconfigure
  :cmd-build
  #'metabuild-sys--cmake-build
  :cmd-clean
  #'metabuild-sys--cmake-clean
  :cmd-test
  #'metabuild-sys--cmake-test
  :cmd-install
  #'metabuild-sys--cmake-install
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-sys-cmake)
;;; metabuild-sys-cmake.el ends here
