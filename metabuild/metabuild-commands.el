;;; metabuild-commands.el --- Operations with build systems

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-22
;; Version: 0.1
;; Keywords: project, convenience, build
;; Homepage:
;; Package-Requires: cl-lib, ido, project, vc

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the commands related code of `metabuild'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'metabuild-core)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild--run-command (name &optional persistent)
  (if-let* ((builder (metabuild-get-current-builder))
            (cmd (metabuild--concat-symbol "metabuild-builder-shell-"  name)))
      (if persistent
          (progn (setq compile-command (funcall cmd builder))
                 (compile compile-command))
        (let ((compile-command (funcall cmd builder)))
          (compile compile-command)))
    (message "irrelevant command")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metabuild-run-configure ()
  "Run the configure command."
  (interactive)
  (metabuild--run-command 'configure)
  (run-hooks 'metabuild-reconfigure-hook))

(defun metabuild-run-reconfigure ()
  "Run the reconfigure command."
  (interactive)
  (metabuild--run-command 'reconfigure)
  (run-hooks 'metabuild-reconfigure-hook))

(defun metabuild-run-build ()
  "Run the build command."
  (interactive)
  (metabuild--run-command 'build t))

(defun metabuild-run-clean ()
  "Run the clean command."
  (interactive)
  (metabuild--run-command 'clean))

(defun metabuild-run-test ()
  "Run the test command."
  (interactive)
  (metabuild--run-command 'test))

(defun metabuild-run-install ()
  "Run the install command."
  (interactive)
  (metabuild--run-command 'install))

(defun metabuild-run-package ()
  "Run the package command."
  (interactive)
  (metabuild--run-command 'package))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'metabuild-commands)
;;; metabuild-commands.el ends here

