;;; dev-env-layout.el --- Customized layout

;; Copyright (C) 2020 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2020-03-02
;; Version: 0.1
;; Keywords: layout
;; Homepage:
;; Package-Requires: (window-purpose)

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Code:
(require 'window-purpose)
(require 'window-purpose-x)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun dev-env-layout--ensure-dedicated-edit (&optional frame)
  "Check if a window for edit purpose is really dedicated. This function removes itself."
  (let ((window (selected-window)))
    (when (and (eq (purpose-window-purpose window) 'edit)
               (not (purpose-window-purpose-dedicated-p window)))
      (progn (purpose-set-window-purpose-dedicated-p window t)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun dev-env-layout-init ()
  ""
  (interactive)
  (progn
    (setq split-height-threshold 1200)
    (setq split-width-threshold 2000)
    (add-to-list 'purpose-user-mode-purposes '(prog-mode . edit))
    (add-to-list 'purpose-user-mode-purposes '(text-mode . edit))
    (add-to-list 'purpose-user-regexp-purposes '("^\\*" . output))
    ;; build it
    (purpose-compile-user-configuration)
    ;; resetting the window layout
    (delete-other-windows)
    ;; make the lone window for edit purpose
    (purpose-set-window-purpose 'edit t)
    ;; split it horizontally
    (split-window-below)
    ;; and select the lower one
    (other-window 1)
    ;; to make it the output window
    (purpose-set-window-purpose 'output t)
    (purpose-set-window-purpose-dedicated-p (selected-window) t)

    ;; killing fucking pu-dummy-* buffers
    (mapc (lambda (buffer)
            (let ((name (buffer-name buffer)))
              (when (string-match "^\\*pu-dummy-.*$" name)
                (kill-buffer buffer))))
          (buffer-list))

    ;; dedicate the window whose purpose is edit when emacs is started with file(s)
    (mapc (lambda (window)
            (when (eq (purpose-window-purpose window) 'edit)
              (purpose-set-window-purpose-dedicated-p window t)
              ))
          (window-list))

    ;; make sure the future windows for edit purpose will be truly dedicated
    (add-hook 'window-selection-change-functions
              'dev-env-layout--ensure-dedicated-edit)

    ;; and.. let's go!
    (purpose-mode)
    (purpose-x-kill-setup)
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'dev-env-layout)
;;; dev-env-layout.el ends here
