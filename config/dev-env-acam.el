;;; dev-env-acam.el --- Support for Auto{conf,make} use

;; Copyright (C) 2020 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-26
;; Version: 0.1
;; Keywords: autoconf automake
;; Homepage:
;; Package-Requires:

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Code:
(defcustom dev-env-acam-user-options
  nil
  "user defined Auto{conf,make} configure parameter list"
  :group 'dev-env
  :type '(repeat string))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun dev-env-acam--project-p (&optional dir)
  "Check if a project contains autogen.sh files."
  (projectile-verify-file "configure" dir))

(projectile-register-project-type 'configure #'dev-env-acam--project-p
                                  :configure "configure"
                                  :compile "bear -- make"
                                  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun dev-env-acam--get-build-dir ()
  "return the Auto{conf,make} build directory relatively to project root"
  (concat (file-name-as-directory "build")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun dev-env-acam--get-configure-command (root-dir)
  "return the Auto{conf,make} configure command according to ROOT-DIR"
  (mapconcat 'identity
             (append (list (concat (file-name-as-directory root-dir)
                                   "configure"))
                     dev-env-acam-user-options)
             " "))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'dev-env-acam)
;;; dev-env-acam.el ends here
