;; inspiration from https://gist.github.com/huytd/6b785bdaeb595401d69adc7797e5c22c

(add-to-list 'load-path "~/.emacs.d/metabuild")
(add-to-list 'load-path "~/.emacs.d/coding-style")
(add-to-list 'load-path "~/.emacs.d/layout")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; package stuff

(package-initialize)

(require 'package)
(require 'use-package)

(setq package-archives '(("melpa" . "http://melpa.org/packages/")
                         ;("gnu" . "http://elpa.gnu.org/packages/")
                         ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; IDO

(use-package ido
  :config
  (setq ido-enable-flex-matching t
        ido-case-fold nil
        ido-show-dot-for-dired nil
        ido-enable-last-directory-history nil
        ido-ignore-extensions t)
  ;; ignore some development related directories
  (push "/build/" completion-ignored-extensions)
  (push "/cross/" completion-ignored-extensions)
  (push "/crossbuild/" completion-ignored-extensions)
  (push ".ccls-cache/" completion-ignored-extensions)
  ;; meson's directories where are .o
  (push ".p/" completion-ignored-extensions)
  (push "~" completion-ignored-extensions)
  (ido-mode 1)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ispell

(use-package ispell
  :config
  (setq ispell-program-name "hunspell")
  ;; configure used dictionary
  (setq ispell-dictionary "fr_FR,en_US")
  ;; ispell-set-spellchecker-params has to be called
  ;; before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "fr_FR,en_US")
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; flyspell

(use-package flyspell
  :config
  :hook
  ;;(prog-mode . flyspell-prog-mode)
  (text-mode . flyspell-mode)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; flycheck-grammalecte mode

(use-package flycheck-grammalecte
  :disabled
  :config
  (grammalecte-download-grammalecte)
  (flycheck-grammalecte-setup)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CCLS

(use-package ccls
  :init
  ;;(setq ccls-args '("--log-file=ccls.log" "-v=1"))
  :hook
  ((c-mode c++-mode) . (lambda () (require 'ccls) (lsp)))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LSP

(use-package lsp-mode
  :init
  (setq lsp-auto-configure t
        lsp-prefer-flymake t
        lsp-prefer-capf t
        lsp-enable-indentation nil
        lsp-enable-on-type-formatting nil
        lsp-enable-snippet t
        lsp-lens-enable nil
        )
  :commands
  lsp
  :config
  (add-hook 'prog-major-mode #'lsp-prog-major-mode-enable)
  :hook
  ((c++-mode c-mode) . lsp)
  )

(use-package lsp-ui
  :after
  lsp-mode
  :config
  (setq lsp-ui-doc-enable nil
        lsp-ui-sideline-show-code-actions nil
        lsp-ui-sideline-show-diagnostics t
        lsp-ui-sideline-ignore-duplicate t)
  (define-key lsp-ui-mode-map (kbd "s-d l") 'lsp-ui-imenu)
  :hook
  (lsp-mode . lsp-ui-mode)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rustic

(use-package rustic
  :disabled
  :after
  lsp-mode
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; company

(use-package company
  :init
  (setq company-minimum-prefix-length 2
        company-auto-complete nil
        company-idle-delay (lambda () (if (company-in-string-or-comment) nil 0.1))
        company-require-match 'never
        tab-always-indent 'complete)
  (defvar completion-at-point-functions-saved nil)
  :hook
  (after-init . global-company-mode)
  :config
  (add-to-list 'company-backends 'company-keywords)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some color in compilation buffer (https://stackoverflow.com/a/63710493)

(use-package xterm-color
  :init
  (setq compilation-environment '("TERM=xterm-256color"))
  (defun my/advice-compilation-filter (f proc string)
    (funcall f proc (xterm-color-filter string)))
  (advice-add 'compilation-filter :around #'my/advice-compilation-filter))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sh

(use-package company-shell
  :after
  company
  :config
  (add-to-list 'company-backends '(company-shell company-shell-env company-fish-shell))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Yasnippet

(use-package yasnippet
  :config
  (yas-global-mode 1)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CMake stuff

(use-package cmake-mode)

(use-package company-cmake
  :after
  company
  :config
  (add-to-list 'company-backends 'company-cmake)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Meson stuff

(use-package meson-mode
  :after
  company
  :defer
  :hook
  (meson-mode . company-mode)
  :config
  (setq meson-indent-basic 4)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metabuild

(defun my:lsp-reinit ()
  ;; update ccls
  (when-let* ((project (project-current))
              (build-path (metabuild-current-builder-build-path))
              (blacklist (metabuild-vc-list-ignored-all project)))
    (progn (setq ccls-initialization-options `(:compilationDatabaseDirectory ,build-path
                                               :index (:initialBlacklist [,@blacklist])))
           (when (lsp-workspaces)
             (lsp-restart-workspace))))
  ;; and lsp
  (when-let* ((project (project-current)))
    (cl-loop for d in (metabuild-vc-list-ignored-dirs project)
             do (add-to-list 'lsp-file-watch-ignored-directories
                             (concat "[/\\\\]"
                                     (string-replace "/"
                                                     "[/\\\\]"
                                                     (directory-file-name d))
                                     "\\'")))))

(use-package metabuild
  :after
  (ccls lsp-mode)
  :bind
  ("C-c b i" . metabuild-initialize-build)
  ("C-c b r" . metabuild-run-reconfigure)
  ("C-c b b" . metabuild-run-build)
  ("C-c b c" . metabuild-run-clean)
  ("C-c b I" . metabuild-run-install)
  ("C-c b P" . metabuild-run-package)
  ("C-c b g" . metabuild-guess)
  ("C-c b f" . metabuild-find-vc-file)
  ("M-p f" . metabuild-find-project-file)
  ("<f9>" . compile)
  ("<f10>" . recompile)
  ("<f11>" . next-error)
  ("<f12>" . previous-error)
  :hook
  (metabuild-reconfigure . my:lsp-reinit)
  :config
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; coding-styles

(use-package coding-style
  :hook
  (metabuild-reconfigure . (lambda ()
                             (when-let* ((root-path (metabuild-current-builder-root-path)))
                               (coding-style-detect root-path))))
  :after
  metabuild
  )

(use-package style-local
  :after
  coding-style
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; layout

(use-package layout
  :hook
  (metabuild-reconfigure . layout-init)
  :after
  metabuild
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; programming languages

(font-lock-add-keywords 'c-mode
                        '(("\\<\\(BUG\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(FAIL\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(FIXME\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(NOTE\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(OPTIMIZE\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(PENDING\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(QUESTION\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(TODO\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(WARNING\\):" 1 font-lock-warning-face prepend)
                          ))

(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.inl\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.l\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.y\\'" . c++-mode))

(font-lock-add-keywords 'c++-mode
                        '(;; < C++11 et >= C++20
                          ("\\<\\(export\\)\\>" . font-lock-keyword-face)
                          ;; C++11
                          ("\\<\\(alignas\\)\\>" . font-lock-keyword-face)
                          ;; C++20
                          ("\\<\\(char8_t\\)\\>" . font-lock-type-face)
                          ("\\<\\(concept\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(consteval\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(constinit\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(co_await\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(co_return\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(co_yield\\)\\>" . font-lock-keyword-face)
                          ;; TM/TS
                          ("\\<\\(atomic_cancel\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(atomic_commit\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(atomic_noexcept\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(refexpr\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(synchronized\\)\\>" . font-lock-keyword-face)
                          ;; Qt
                          ("\\<\\(Q_OBJECT\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(Q_SIGNAL\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(Q_SIGNALS\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(Q_SLOT\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(Q_SLOTS\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(Q_EMIT\\)\\>" . font-lock-keyword-face)
                          ("\\<\\(Q_PROPERTY\\)\\>" . font-lock-keyword-face)
                          ;; me
                          ("\\<\\(BUG\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(FAIL\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(FIXME\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(NOTE\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(OPTIMIZE\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(PENDING\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(QUESTION\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(TODO\\):" 1 font-lock-warning-face prepend)
                          ("\\<\\(WARNING\\):" 1 font-lock-warning-face prepend)
                          ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GLSL/CUDA

(use-package glsl-mode
  :mode
  ("\\.glsl\\'" "\\.vert\\'" "\\.frag\\'" "\\.geom\\'")
  )

(use-package company-glsl
  :after
  (company glsl-mode)
  :config
  (add-to-list 'company-backends 'company-glsl)
  )

(use-package cuda-mode
  :disabled
  :mode
  ("\\.cu\\'")
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Qt ui/rcc

(use-package nxml-mode
  :mode
  (("\\.ui\\'" . xml-mode)
   ("\\.qrc\\'" . xml-mode))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Qt qss

(use-package css-mode
  :mode
  ("\\.qss\\'")
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Markdown

(use-package markdown-mode
  :mode
  ("\\.md\\'")
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; config bootstrap

(global-set-key (kbd "C-c C-c C-b")
                #'(lambda ()
                    (interactive)
                    (progn (package-refresh-contents)
                           (package-install-selected-packages))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; correct short-cuts...

;;; window focus switch
(global-set-key (kbd "<C-tab>")
                'other-window)

(global-set-key (kbd "C-S-<iso-lefttab>")
                #'(lambda ()
                    (interactive)
                    (other-window -1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; general configuration

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(compilation-skip-threshold 2)
 '(cua-mode t)
 '(current-language-environment "UTF-8")
 '(electric-pair-mode t)
 '(fringe-mode '(nil . 0) nil (fringe))
 '(global-font-lock-mode t)
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries 'left)
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(overflow-newline-into-fringe t)
 '(package-selected-packages
   '(cmake-mode flymake-shellcheck meson-mode xterm-color adoc-mode ccls company company-glsl company-jedi company-shell flycheck lsp-ui markdown-mode smart-tabs-mode window-purpose yasnippet))
 '(show-paren-mode t)
 '(show-trailing-whitespace t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(region ((t (:background "gray80")))))
(put 'downcase-region 'disabled nil)
