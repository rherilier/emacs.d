;;; coding-style.el ---

;; Copyright (C) 2024 Rémi Hérilier

;; Author: Rémi Hérilier <rherilier@yahoo.fr>
;; Created: 2024-10-22
;; Version: 0.1
;; Keywords: c-mode, c++-mode
;; Homepage:
;; Package-Requires:

;; This file is not part of GNU Emacs.

;; This file is free software... see <http://www.wtfpl.net/>.

;;; Commentary:

;; This file contains the core code of `coding-style'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-defstruct style
  id
  desc
  matches
  init
  uninit)

(defvar styles-alist nil
  "list of styles id, order them according to their :matches values (the more precice first).
It contains all of the style configuration that are currently registered.")

(defvar styles-map (make-hash-table :test 'eql)
  "styles, order them according to their :matches values (the more precice first).
It contains all of the style configuration that are currently registered.")

(defvar style-current-id nil
  "current style")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun coding-style--register (style)
  "Register a new style CONFIG"
  (puthash (style-id style) style styles-map)
  (add-to-list 'styles-alist style t))

(defun coding-style--matchs? (root-dir matches)
  "test if ROOT-DIR matches one of MATCHES"
  (cl-some (lambda (match)
             (string-match match root-dir))
           matches))

(defun coding-style--eval (form)
  "evaluate FORM whathever it is an function or a list of functions"
  (cond
   ((atom (car form))
    (eval form))
   ((list (car form))
    (dolist (expr form)
      (eval expr)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro coding-style-register-style (id &rest args)
  "Register a new coding style.
Usage:

  (coding-style-register-style id
    [:keyword [option]]...)

:id        The coding style's name
:desc      The coding style's description
:matches   The substring set to accept this coding style
:init      Something to call at initialization time
:uninit    Something to call at deinitialization time
:disabled  To disable it"
  (declare (indent defun))
  (unless (memq :disabled args)
    `(coding-style--register (make-style :id ,id ,@args))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun coding-style-detect (root-dir)
  "initialize project style according to ROOT-DIR"
  (let ((result nil))
    (dolist (config styles-alist)
      (when (coding-style--matchs? root-dir (style-matches config))
        (when style-current-id
          (coding-style--eval (style-uninit (gethash style-current-id
                                                     styles-map))))
        (message "applying style %s" (style-id config))
        (coding-style--eval (style-init config))
        (setq style-current-id (style-id config)
              result t)))
    (unless result
      (message "no style found for '%s', using default" root-dir))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'coding-style)
;;; coding-style.el ends here
