# my Emacs config

## Shortcuts

* `C-tab` and `C-S-tab`: move from a window to another;
* `super-d l`: ui-menu;
* `C-c C-c C-b`: emacs configuration bootstrap;
* `M-p f`: find project file;
* `C-c b f`: find versioned file;
* `C-c b i`: initialize a new build;
* `C-c b r`: refresh/clean the build configuration;
* `C-c b b`: run a build;
* `C-c b c`: cleanr a build;
* `C-c b t`: run the tests;
* `C-c b I`: install the project;
* `C-c b P`: package it;
* `f9`: compile;
* `f10`: recompile;
* `f11`: next error;
* `f12`: previous error;

## Emacs dependencies

* use-package;
* company-lsp ccls company-glsl company-shell company-jedi lsp-ui yasnippet;
* window-purpose;
* smart-tabs-mode blank-mode;
* cuda-mode glsl-mode adoc-mode.

## System dependencies

* emacs :o)
* ccls for C/C++ code completion;
* bear to have code completion on pure Makefile C/C++ projects;
* python3-jedi for Python code completion;
* many others to write code O:-,

## TODO

* make coding-style/*.el uses a dedicated file for each style and make it store the project/path instead of the actual hard-coded list;
* move from lsp-mode to eglot.

## links

* better C style handling (see https://www.emacswiki.org/emacs/ProjectSettings)
* https://emacs.stackexchange.com/questions/21416/how-to-force-completions-buffer-to-appear-in-a-side-window
